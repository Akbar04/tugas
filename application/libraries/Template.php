<?php

class Template{
	protected $_ci;
	function __construct(){
		$this->_ci=&get_instance();
	}

	function display($template,$data=NULL){
		//$data['_top_menu']=$this->_ci->load->view('admin_template/top_menu',$data,TRUE);
		$data['_bottom']=$this->_ci->load->view('admin_template/bottom',$data,TRUE);
		$data['_side_menu']=$this->_ci->load->view('admin_template/side_menu',$data,TRUE);
		$data['_content']=$this->_ci->load->view($template,$data,TRUE);
		$this->_ci->load->view('/template.php',$data);
	}
	
	
}
?>