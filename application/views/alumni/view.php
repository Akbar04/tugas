<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">Panel heading</div>

  <!-- Table -->
  <table class="table">
    <tr>
      <th>Nama</th>
      <td><?php echo $identitas['name']; ?></td>
    </tr>
    <tr>
      <th>NIM</th>
      <td><?php echo $identitas['nim']; ?></td>
    </tr>
    <tr>
      <th>Gender</th>
      <td><?php echo $identitas['gender']; ?></td>
    </tr>
    <tr>
      <th>Judul Skripsi</th>
      <td><?php echo $identitas['thesis_title']; ?></td>
    </tr>
    <tr>
      <th>Lama Pengerjaan Skripsi</th>
      <td><?php echo $identitas['long_thesis_work']; ?></td>
    </tr>
    <tr>
      <th>Tanggal SK Pembimbing</th>
      <td><?php echo $identitas['tanggal_sk_pembimbing']; ?></td>
    </tr>
    <tr>
      <th>Nomor SK Pembimbing</th>
      <td><?php echo $identitas['nomor_sk_pembimbing']; ?></td>
    </tr>
    <tr>
      <th>Pembimbing Utama</th>
      <td><?php echo $dosen['name_pembimbing_utama']; ?></td>
    </tr>
    <tr>
      <th>NIP</th>
      <td><?php echo $dosen['nim_pembimbing_utama']; ?></td>
    </tr>
    <tr>
      <th>Pembimbing pertama</th>
      <td><?php echo $dosen['name_pembimbing_pertama']; ?></td>
    </tr>
    <tr>
      <th>NIP</th>
      <td><?php echo $dosen['nim_pembimbing_pertama']; ?></td>
    </tr>
    <tr>
      <th>Tanggal Daftar</th>
      <td><?php echo $kuliah['tanggal_daftar']; ?></td>
    </tr>
    <tr>
      <th>Tanggal Sidang</th>
      <td><?php echo $kuliah['tanggal_sidang']; ?></td>
    </tr>
    <tr>
      <th>Semester Lulus</th>
      <td>Genap-2018/2019</td>
    </tr>
    <tr>
      <th>Tanggal dan Nilai Seminar 1</th>
      <td><?php echo $kuliah['tanggal_dan_nilai_seminar1']; ?></td>
    </tr>
    <tr>
      <th>Tanggal dan Nilai Seminar 2</th>
      <td><?php echo $kuliah['tanggal_dan_nilai_seminar2']; ?></td>
    </tr>
    <tr>
      <th>Nilai Sidang</th>
      <td><?php echo $kuliah['nilai_sidang']; ?></td>
    </tr>
  </table>
</div>
