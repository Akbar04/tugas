<div class="panel panel-primary">
  <!-- Default panel contents -->
  <div class="panel-heading">Data Mahasiswa</div>

  <!-- Table -->
  <table class="table">
    <tr>
      <div class="input-group">
        <th colspan="2">
          <span class="input-group-addon" id="basic-addon1">
            Nama Mahasiswa
          </span>
        </th>
        <td>
          <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
        </td>
      </div>
    </tr>

    <tr>
      <div class="input-group">
        <th colspan="2">
          <span class="input-group-addon" id="basic-addon1">
            NIM
          </span>
        </th>
        <td>
          <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
        </td>
      </div>
    </tr>

    <tr>
      <div class="input-group">
        <th colspan="2">
          <span class="input-group-addon" id="basic-addon1">
            Jenis Kelamin
          </span>
        </th>
        <td>
          <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
        </td>
      </div>
    </tr>

    <tr>
      <div class="input-group">
        <th colspan="2">
          <span class="input-group-addon" id="basic-addon1">
            Judul Skripsi
          </span>
        </th>
        <td>
          <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
        </td>
      </div>
    </tr>

    <tr>
      <div class="input-group">
        <th colspan="2">
          <span class="input-group-addon" id="basic-addon1">
            Lama Pengerjaan Skripsi
          </span>
        </th>
        <td>
          <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
        </td>
      </div>
    </tr>

    <tr>
      <div class="input-group">
        <th colspan="2">
          <span class="input-group-addon" id="basic-addon1">
            Tanggal SK Pembimbing
          </span>
        </th>
        <td>
          <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
        </td>
      </div>
    </tr>

    <tr>
      <div class="input-group">
        <th colspan="2">
          <span class="input-group-addon" id="basic-addon1">
            Pembimbing
          </span>
        </th>
      </div>
    </tr>

    <tr>
      <div class="input-group">
        <th></th>
        <th>
          <span class="input-group-addon" id="basic-addon1">
            Nomor SK Pembimbing
          </span>
        </th>
        <td>
          <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
        </td>
      </div>
    </tr>

    <tr>
      <div class="input-group">
        <th>
          <span class="input-group-addon" id="basic-addon1">
            Pembimbing Utama
          </span>
        </th>
        <td>
          <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
        </td>
      </div>
    </tr>

    <tr>
      <div class="input-group">
        <th>
          <span class="input-group-addon" id="basic-addon1">
            NIP
          </span>
        </th>
        <td>
          <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
        </td>
      </div>
    </tr>

    <tr>
      <div class="input-group">
        <th>
          <span class="input-group-addon" id="basic-addon1">
            Pembimbing Pertama
          </span>
        </th>
        <td>
          <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
        </td>
      </div>
    </tr>

    <tr>
      <div class="input-group">
        <th>
          <span class="input-group-addon" id="basic-addon1">
            NIP
          </span>
        </th>
        <td>
          <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
        </td>
      </div>
    </tr>

    <tr>
      <div class="input-group">
        <th>
          <span class="input-group-addon" id="basic-addon1">
            Penguji Ujian Sidang 3
          </span>
        </th>
        <td>
          <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
        </td>
      </div>
    </tr>

    <tr>
      <div class="input-group">
        <th>
          <span class="input-group-addon" id="basic-addon1">
            Tanggal Daftar
          </span>
        </th>
        <td>
          <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
        </td>
      </div>
    </tr>

    <tr>
      <div class="input-group">
        <th>
          <span class="input-group-addon" id="basic-addon1">
            Tanggal Sidang
          </span>
        </th>
        <td>
          <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
        </td>
      </div>
    </tr>

    <tr>
      <div class="input-group">
        <th>
          <span class="input-group-addon" id="basic-addon1">
            Semester Lulus
          </span>
        </th>
        <td>
          <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
        </td>
      </div>
    </tr>

    <tr>
      <div class="input-group">
        <th>
          <span class="input-group-addon" id="basic-addon1">
            Tanggal dan Nilai Seminar 1
          </span>
        </th>
        <td>
          <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
        </td>
      </div>
    </tr>

    <tr>
      <div class="input-group">
        <th>
          <span class="input-group-addon" id="basic-addon1">
            Tanggal dan Nilai Seminar 2
          </span>
        </th>
        <td>
          <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
        </td>
      </div>
    </tr>

    <tr>
      <div class="input-group">
        <th>
          <span class="input-group-addon" id="basic-addon1">
            Nilai Sidang
          </span>
        </th>
        <td>
          <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1">
        </td>
      </div>
    </tr>
    <tr>
      <td> <a href="<?php ?>" type="button" name="submit" class="btn btn-success">Kirim</a> </td>
    </tr>
  </table>
</div>
