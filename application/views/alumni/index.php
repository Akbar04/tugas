<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">
    <div class="row">
      <div class="col-md-5">
        <h1>DATA ALUMNI
          <a href="<?php echo site_url('identitas/create') ?>" type="button" class="btn btn-primary" data-toggle="tooltip" data-placement="right" title="Tooltip on right">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
          </a>
        </h1>
      </div>
    </div>
  </div>

  <!-- Table -->
  <table class="table">
    <tr>
      <th>Nomor</th>
      <th>Nama</th>
      <th>NIM</th>
      <th>Gender</th>
      <th>Judul Skripsi</th>
      <th>keterangan</th>
    </tr>
    <?php foreach ($identitas as $data_identitas): ?>
      <tr>
        <td><?php echo $data_identitas['id']; ?></td>
        <td><?php echo $data_identitas['name']; ?></td>
        <td><?php echo $data_identitas['nim']; ?></td>
        <td><?php echo $data_identitas['gender']; ?></td>
        <td><?php echo $data_identitas['thesis_title']; ?></td>
        <td> <a href="<?php echo site_url('identitas/view/'.$data_identitas['id']) ?>" type="button" name="submit" class="btn btn-info">Detail</a> </td>
      </tr>
    <?php endforeach; ?>
  </table>
</div>
