<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Detail  </h3>
  </div>
  <div class="panel-body">
    <table class="table">
      <tr>
        <th>HARI</th>
        <td><?php echo $tagenda['hari']; ?></td>
      </tr>
      <tr>
        <th>Tanggal Agenda</th>
        <td><?php echo $tagenda['tgl_agenda']; ?></td>
      </tr>
      <tr>
        <th>Jam</th>
        <td><?php echo $tagenda['jam']; ?></td>
      </tr>
      <tr>
        <th>Agenda</th>
        <td><?php echo $tagenda['agenda']; ?></td>
      </tr>
      <tr>
        <th>Tempat</th>
        <td><?php echo $tagenda['tempat']; ?></td>
      </tr>
      <tr>
        <th>Keterangan</th>
        <td><?php echo $tagenda['keterangan']; ?></td>
      </tr>
    </table>
  </div>
</div>
