<div class="alert alert-danger" role="alert">
  <?php echo validation_errors(); ?>
</div>

<?php echo form_open('tagenda/update/'.$tagenda['id_agenda']); ?>
<div class="panel panel-primary">
  <!-- Default panel contents -->
  <div class="panel-heading">
    TAMBAH DATA
  </div>

  <!-- Table -->
  <table class="table">
    <tr>
      <div class="input-group">
        <th>
          <span class="input-group-addon" id="basic-addon1" for="hari">
            hari
          </span>
        </th>
        <td>
          <input type="text" class="form-control" placeholder="Hari" aria-describedby="basic-addon1", name="hari" value="<?php echo $tagenda['hari']; ?>">
        </td>
        </div>
    </tr>

    <tr>
      <div class="input-group">
        <th>
          <span class="input-group-addon" id="basic-addon1" for="tgl_agenda">
            Tanggal Agenda
          </span>
        </th>
        <td>
          <input type="date" class="form-control" placeholder="Hari" aria-describedby="basic-addon1", name="tgl_agenda" value="<?php echo $tagenda['tgl_agenda']; ?>">
        </td>
      </div>
    </tr>

    <tr>
      <div class="input-group">
        <th>
          <span class="input-group-addon" id="basic-addon1" for="jam">
            Jam
          </span>
        </th>
        <td>
          <input type="time" class="form-control" placeholder="Hari" aria-describedby="basic-addon1", name="jam" value="<?php echo $tagenda['jam']; ?>">
        </td>
      </div>
    </tr>

    <tr>
      <div class="input-group">
        <th>
          <span class="input-group-addon" id="basic-addon1" for="agenda">
            Agenda
          </span>
        </th>
        <td>
          <input type="text" class="form-control" placeholder="Agenda Acara" aria-describedby="basic-addon1", name="agenda" value="<?php echo $tagenda['agenda']; ?>">
        </td>
      </div>
    </tr>

    <tr>
      <div class="input-group">
        <th>
          <span class="input-group-addon" id="basic-addon1" for="tempat">
            Tempat
          </span>
        </th>
        <td>
          <input type="text" class="form-control" placeholder="Tempat Acara" aria-describedby="basic-addon1", name="tempat" value="<?php echo $tagenda['tempat']; ?>">
        </td>
      </div>
    </tr>

    <tr>
      <div class="input-group">
        <th>
          <span class="input-group-addon" id="basic-addon1" for="keterangan">
            Keterangan
          </span>
        </th>
        <td>
          <input type="text" class="form-control" placeholder="Keterangan" aria-describedby="basic-addon1", name="keterangan" value="<?php echo $tagenda['keterangan']; ?>">
        </td>
      </div>
    </tr>

    <tr>
      <div class="input-group">
        <td>
          <input type="submit" name="submit" value="KIRIM" class="btn btn-primary">
        </td>
      </div>
    </tr>
  </table>
</div>
<?php echo form_close(); ?>
