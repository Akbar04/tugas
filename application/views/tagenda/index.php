<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading">
    <div class="row">
      <div class="col-md-5">
        <h3>D A T A
          <a href="<?php echo site_url('tagenda/create') ?>" type="button" class="btn btn-primary" data-toggle="tooltip" data-placement="right" title="Tooltip on right">
            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
          </a>
        </h3>
        <?php if($this->session->flashdata('success')): ?>
          <div class="alert alert-primary" role="alert">
            <?php echo $this->session->flashdata('success'); ?>
          </div>
        <?php endif ?>
      </div>
    </div>
  </div>

  <!-- Table -->
  <table class="table">
    <tr>
      <th>Nomor</th>
      <th>Hari</th>
      <th>Tanggal</th>
      <th>Jam</th>
      <th>Agenda</th>
      <th>Tempat</th>
      <th>keterangan</th>
      <th colspan="3"></th>
    </tr>
    <?php foreach ($tagenda as $tagendas): ?>
      <tr>
        <td><?php echo $tagendas['id_agenda']; ?></td>
        <td><?php echo $tagendas['hari']; ?></td>
        <td><?php echo $tagendas['tgl_agenda']; ?></td>
        <td><?php echo $tagendas['jam']; ?></td>
        <td><?php echo $tagendas['agenda']; ?></td>
        <td><?php echo $tagendas['tempat']; ?></td>
        <td><?php echo $tagendas['keterangan']; ?></td>
        <td> <a href="<?php echo site_url('tagenda/view/'.$tagendas['id_agenda']) ?>" type="button" name="submit" class="btn btn-info">Detail</a> </td>
        <td> <a href="<?php echo site_url('tagenda/edit/'.$tagendas['id_agenda']) ?>" type="button" name="submit" class="btn btn-warning">Edit</a> </td>
        <td> <a  type="button" name="submit" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal">Delete</a> </td>
      </tr>
    <?php endforeach; ?>
  </table>
  <nav aria-label="Page navigation example">
    <?php echo $this->pagination->create_links(); ?>
  </nav>

</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h5 class="modal-title" id="exampleModalLabel">Hapus Data</h5>

      </div>
      <div class="modal-body">
        Apakah anda yakin ingin menghapus data ini ?
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-primary" href="<?php echo site_url('tagenda/delete/'.$tagendas['id_agenda']) ?>">YES</a>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">NO</button>
      </div>
    </div>
  </div>
</div>
