<?php echo form_open('tagenda/store'); ?>
<div class="panel panel-danger">
  <!-- Default panel contents -->
  <div class="panel-heading">
    TAMBAH DATA
  </div>

  <div class="form-group">
    <label for="hari">Hari</label>
    <input type="text" class="form-control" id="hari" aria-describedby="hari" placeholder="Hari Acara", name="hari">
    <small id="hari" class="form-text text-muted">
      <?php echo validation_errors('hari'); ?>
    </small>
  </div>

  <div class="form-group">
    <label for="tgl_agenda">Tanggal Agenda</label>
    <input type="date" class="form-control" id="tgl_agenda" aria-describedby="tgl_agenda" placeholder="Enter email", name="tgl_agenda">
    <small id="tgl_agenda" class="form-text text-muted">
      <?php echo validation_errors(); ?>
    </small>
  </div>

  <div class="form-group">
    <label for="jam">Waktu</label>
    <input type="time" class="form-control" id="jam" aria-describedby="jam" placeholder="Enter email", name="jam">
    <small id="jam" class="form-text text-muted">
      <?php echo validation_errors(); ?>
    </small>
  </div>

  <div class="form-group">
    <label for="agenda">Agenda</label>
    <input type="text" class="form-control" id="agenda" aria-describedby="agenda" placeholder="Agenda Acara", name="agenda">
    <small id="agenda" class="form-text text-muted">
      <?php echo validation_errors(); ?>
    </small>
  </div>

  <div class="form-group">
    <label for="tempat">Tempat</label>
    <input type="text" class="form-control" id="Tempat" aria-describedby="agenda" placeholder="Tempat Acara", name="tempat">
    <small id="Tempat" class="form-text text-muted">
      <?php echo validation_errors(); ?>
    </small>
  </div>

  <div class="form-group">
    <label for="keterangan">Keterangan</label>
    <input type="text" class="form-control" id="keterangan" aria-describedby="keterangan" placeholder="Keterangan Acara", name="keterangan">
    <small id="keterangan" class="form-text text-muted">
      <?php echo validation_errors(); ?>
    </small>
  </div>

  <div class="input-group">
    <td>
      <input type="submit" name="submit" value="KIRIM" class="btn btn-primary">
    </td>
  </div>
</div>
<?php echo form_close(); ?>
