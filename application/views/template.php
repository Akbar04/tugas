<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>.: Data Alumni :.</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/adminlte/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/adminlte/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo base_url();?>css/my_style_admin.css">
  <link href="<?php echo base_url();?>assets/plugins/jquery_confirm/css/jquery-confirm.css" rel="stylesheet">

  <script src="<?php echo base_url();?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/adminlte/js/app.min.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/adminlte/js/demo.js"></script>

  <script src="<?php echo base_url();?>js/jquery-ui.js"></script>
  <script src="<?php echo base_url();?>assets/plugins/jquery_confirm/js/jquery-confirm.js"></script>

  <script type="text/javascript">
    var base_url = "<?php echo base_url();?>";
  </script>

  <style>
  .panel-heading > .tombol-kanan {margin-top: -5px; float: right;}
  .panel-heading > .tombol-kanan > .btn-xs {  margin-top: 4px;}
  </style>




</head>
<body class="hold-transition skin-red sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url();?>" class="logo">
      <span class="logo-mini">
        <img src="https://upload.wikimedia.org/wikipedia/id/9/95/Logo_UH.png" height="40" width="45">
      </span>
      <span class="logo-lg"><img src="https://upload.wikimedia.org/wikipedia/id/9/95/Logo_UH.png" height="40" width="45"><b>UNHAS</b></span>
    </a>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>
       <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li>
            <!--<?php echo anchor('admpassword','<i class="fa fa-key"></i> Ganti Password');?> -->

          </li>
          <li>
         <!--   <?php echo anchor('administrator/logout', '<i class="fa fa-sign-out"></i> L o g o u t');?> -->
          </li>
        </ul>
    </nav>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
      <div class="txt-selamat-datang">
        Selamat Datang Admin,
        <a href="http://localhost/tugas/auth/logout">logout</a>
       </div>
      <ul class="sidebar-menu" data-widget="tree">
        <?php echo $_side_menu;?>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
      <div class="box box-danger">
        <div class="box-body">
          <?php echo $_content;?>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <?php echo $_bottom ?>
  </footer>
</div>
<!-- ./wrapper -->



</body>
</html>
