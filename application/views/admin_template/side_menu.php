<li class="active treeview">
	<a href="#">
	    <i class="fa fa-dashboard"></i> <span>Beranda</span>
	    <span class="pull-right-container">
	      <i class="fa fa-angle-left pull-right"></i>
	    </span>
	</a>
	<ul class="treeview-menu  ">
		<li>
			<?php echo anchor('identitas/','<i class="fa fa-clone text-success"></i> Data Alumni');?>
		</li>
		<li>
			<?php echo anchor('tagenda/index','<i class="fa fa-clone text-success"></i> Agenda');?>
		</li>
	</ul>
</li>
