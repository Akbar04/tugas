-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 09, 2018 at 10:20 AM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kampus`
--

-- --------------------------------------------------------

--
-- Table structure for table `dosen`
--

CREATE TABLE `dosen` (
  `id` int(128) NOT NULL,
  `name_pembimbing_utama` varchar(255) NOT NULL,
  `nim_pembimbing_utama` int(50) NOT NULL,
  `name_pembimbing_pertama` varchar(255) NOT NULL,
  `nim_pembimbing_pertama` int(50) NOT NULL,
  `penguji_ujian_sidang_tiga` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dosen`
--

INSERT INTO `dosen` (`id`, `name_pembimbing_utama`, `nim_pembimbing_utama`, `name_pembimbing_pertama`, `nim_pembimbing_pertama`, `penguji_ujian_sidang_tiga`) VALUES
(1, 'illang', 12938, 'sule', 3284792, 'abi'),
(2, 'andhi', 1293810, 'rio', 1923840, 'zinedine');

-- --------------------------------------------------------

--
-- Table structure for table `identitas`
--

CREATE TABLE `identitas` (
  `id` int(128) NOT NULL,
  `name` varchar(255) NOT NULL,
  `nim` int(50) NOT NULL,
  `gender` varchar(11) NOT NULL,
  `thesis_title` varchar(128) NOT NULL,
  `long_thesis_work` varchar(10) NOT NULL,
  `tanggal_sk_pembimbing` date NOT NULL,
  `nomor_sk_pembimbing` int(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `identitas`
--

INSERT INTO `identitas` (`id`, `name`, `nim`, `gender`, `thesis_title`, `long_thesis_work`, `tanggal_sk_pembimbing`, `nomor_sk_pembimbing`) VALUES
(1, 'akbar', 13116303, 'laki-laki', 'aplikasi menghilankan kejombloan', '3 bulan', '2018-04-03', 46299421);

-- --------------------------------------------------------

--
-- Table structure for table `kuliah`
--

CREATE TABLE `kuliah` (
  `id` int(11) NOT NULL,
  `tanggal_daftar` date NOT NULL,
  `tanggal_sidang` date NOT NULL,
  `tanggal_dan_nilai_seminar1` varchar(128) NOT NULL,
  `tanggal_dan_nilai_seminar2` varchar(128) NOT NULL,
  `nilai_sidang` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tagenda`
--

CREATE TABLE `tagenda` (
  `id_agenda` int(11) NOT NULL,
  `hari` varchar(15) NOT NULL,
  `tgl_agenda` date NOT NULL,
  `jam` time NOT NULL,
  `agenda` varchar(200) NOT NULL,
  `tempat` varchar(50) NOT NULL,
  `keterangan` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tagenda`
--

INSERT INTO `tagenda` (`id_agenda`, `hari`, `tgl_agenda`, `jam`, `agenda`, `tempat`, `keterangan`) VALUES
(1, 'jum\'at', '2018-04-03', '01:00:00', 'enjak leher', 'T', 'semua harus hadir'),
(2, 'senin', '2000-01-03', '00:00:00', 'bakar ikan', 'rappang', 'eaa');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `identitas`
--
ALTER TABLE `identitas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kuliah`
--
ALTER TABLE `kuliah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tagenda`
--
ALTER TABLE `tagenda`
  ADD PRIMARY KEY (`id_agenda`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dosen`
--
ALTER TABLE `dosen`
  MODIFY `id` int(128) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `identitas`
--
ALTER TABLE `identitas`
  MODIFY `id` int(128) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `kuliah`
--
ALTER TABLE `kuliah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tagenda`
--
ALTER TABLE `tagenda`
  MODIFY `id_agenda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
