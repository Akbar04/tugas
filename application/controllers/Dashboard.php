<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->library('template');
		$this->load->library('session');
	}

	public function index()
	{
		$this->session->set_flashdata('message', 'Login Berhasil');
		$this->template->display('dashboard');
	}

}
