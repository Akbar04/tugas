<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tagenda extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->library('template');
    $this->load->model('tagenda_model');
    $this->load->helper('url_helper');
		$this->load->library('session');
		$this->load->library('pagination');
	}

	public function index()
	{
		$jumlahdata = $this->tagenda_model->jumlahData();
		$config['base_url'] = base_url().'tagenda/index';
		$config['total_rows'] = $jumlahdata;
		$config['per_page'] = 5	;
		$from = $this->uri->segment(3);
		$this->pagination->initialize($config);
		$data['tagenda'] = $this->tagenda_model->get_tagenda(false,$config['per_page'], $from);
		$this->template->display('tagenda/index',$data);
	}

  public function view($id = NULL)
  {
    $data['tagenda'] = $this->tagenda_model->get_tagenda($id);

		$this->template->display('tagenda/view',$data);
  }

  public function create(){
    $this->load->helper('form');
    $this->template->display('tagenda/create');
  }

  public function store(){

    $this->load->library('form_validation');
    $this->form_validation->set_rules('hari', 'hari', 'required');
    $this->form_validation->set_rules('tgl_agenda', 'tgl_agenda', 'required');
    $this->form_validation->set_rules('jam', 'jam', 'required');
    $this->form_validation->set_rules('agenda', 'agenda', 'required');
    $this->form_validation->set_rules('tempat', 'tempat', 'required');
    $this->form_validation->set_rules('keterangan', 'keterangan', 'required');
    if($this->form_validation->run() === FALSE){
      $this->template->display('tagenda/create');
    }
    else{
      $this->tagenda_model->set_tagenda();
			$this->session->set_flashdata('success','Data Berhasil Tersimpan.');
      redirect('tagenda/index');
    }
  }

	public function edit($id){
		$this->load->helper('form');
		$data['tagenda'] = $this->tagenda_model->get_tagenda($id);
		$this->template->display('tagenda/edit',$data);
	}

	public function update($id){
		$this->load->library('form_validation');
    $this->form_validation->set_rules('hari', 'hari', 'required');
    $this->form_validation->set_rules('tgl_agenda', 'tgl_agenda', 'required');
    $this->form_validation->set_rules('jam', 'jam', 'required');
    $this->form_validation->set_rules('agenda', 'agenda', 'required');
    $this->form_validation->set_rules('tempat', 'tempat', 'required');
    $this->form_validation->set_rules('keterangan', 'keterangan', 'required');
    if($this->form_validation->run() === FALSE){
      $this->template->display('tagenda/edit');
    }
    else{
      $this->tagenda_model->update($id);
      redirect('tagenda/index');
    }
	}

	public function delete($id){
		$data['tagenda'] = $this->tagenda_model->delete($id);
		redirect('tagenda/index',$data);
	}
}
