<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
	{
		parent::__construct();
		$this->load->library('template');
		//$this->load->model('model_dosen');
		//$this->load->helper('xinha');
	}

	

	public function index()
	{

		$this->load->helper('form');
		$is_submit = $this->input->post('is_submit');
		//$this->load->library('fungsiku');
		//$this->load->library('access');
		if ($is_submit == 1)
		{

			$this->load->library('form_validation');
			$this->form_validation->set_rules('nim','nama','required');

				if ($this->form_validation->run() == FALSE)
				{
					$data = array();
					$data['message'] ='<span style="color:red">'.validation_errors().'</span>';
					$data['mahasiswa'] = array(
						'nim'=>$this->input->post('nim'),
						'nama'=>$this->input->post('nama'),
						'jeniskelamin'=>$this->input->post('jeniskelamin'),
						'no_hp'=>$this->input->post('no_hp'),
						'email'=>$this->input->post('email'),
						'prodi'=>$this->input->post('prodi')
						);

					$this->temp_admin->display('admin/dosen/view_edit',$data);

				} else
				{

					$data['id_dosen'] = $this->input->post('id_dosen');
					$data['nama_dosen'] = $this->input->post('nama_dosen');
					$data['deskripsi'] = $this->input->post('deskripsi');
					$data['jabatan'] = $this->input->post('jabatan');
					$data['email'] = $this->input->post('email');
					$data['nip'] = $this->input->post('nip');
					$data['flag'] = $this->input->post('flag');
					$data['operator'] = $this->access->get_user_id();
					if ($gambar['file_name'] != "")
					{
					$data['file'] = $gambar['file_name'];
					}


					$res = $this->model_dosen->dosen_baru($data);

					$data = array();
					if ($res)
					{
						$data['dosen'] = array(
						'id_dosen' => '',
						'file' => '',
						'nama_dosen'=>'',
						'jabatan'=>'',
						'deskripsi'=>'',
						'nip'=>'',
						'email'=>'',
						'flag'=>''
						);
					$data['kode'] = '';
					$data['message'] = $this->fungsiku->dialog_sukses('Data berhasil disimpan');
					} else
					{
					$data['dosen'] = array(
						'id_dosen'=>$this->input->post('id_key'),
						'file' => $gambar['file_name'],
						'nama_dosen'=>$this->input->post('nama_dosen'),
						'deskripsi'=>$this->input->post('deskripsi'),
						'jabatan'=>$this->input->post('jabatan'),
						'email'=>$this->input->post('email'),
						'nip'=>$this->input->post('nip'),
						'flag'=>$this->input->post('flag')
						);

					$data['kode'] = '';
					$data['message'] = $this->fungsiku->dialog_gagal('Data gagal disimpan');
					}
					$data['xinha_inclusion'] = javascript_xinha(array('deskripsi'),
	                                               array('ImageManager',
							'InsertSmiley'),
							"blue-look");

					$data['url'] = 'admdosen/baru';
					$this->temp_admin->display('admin/dosen/view_edit',$data);

				}

		} else
		{
		$data['dosen'] = array(
						'id_dosen' => '',
						'file' => '',
						'nama_dosen'=>'',
						'jabatan'=>'',
						'deskripsi'=>'',
						'email'=>'',
						'flag'=>'',
						'nip'=>''
						);
		$data['kode'] = '';
		$data['message'] = '';
		$this->temp_admin->display('admin/dosen/view_edit',$data);
		}
	}









		
	function hapus($kunci)
	{
		$this->load->library('fungsiku');

		$res = $this->model_dosen->hapusdosen($kunci);
		redirect("admdosen/index/0/".$res);

	}


	function edit($kode)
	{

		$this->load->library('fungsiku');
		$this->load->library('access');
		$this->load->helper('form');
		$is_submit = $this->input->post('is_submit');
		if ($is_submit == 1)
		{

			$this->load->library('upload');

			$config['upload_path'] = './appimages/';
			$config['allowed_types'] = 'jpg|gif|png|tif';

			$this->upload->initialize($config);
			$st = $this->upload->do_upload('file');
			$gambar = $this->upload->data();
			if ($st)
			{
			$config = array(
				  'source_image' => $gambar['full_path'], //get original image
				  'new_image' => './appimages/thumbnails/', //save as new image //need to create thumbs first
				  'maintain_ratio' => true,
				  'width' => 1000,
				  'height' =>150

				);
				$this->load->library('image_lib', $config); //load library
				$this->image_lib->resize(); //do whatever specified in config
			}

			$this->load->library('form_validation');
			$this->form_validation->set_rules('id_dosen','Kode Dosen','required');
			$this->form_validation->set_rules('nama_dosen','Nama Dosen','required');

			if ($this->form_validation->run() == FALSE)
			{
				$data = array();
				$data['message'] ='<span style="color:red">'.validation_errors().'</span>';
				$data['dosen'] = array(
					'id_dosen'=>$this->input->post('id_key'),
					'file' => $gambar['file_name'],
					'nama_dosen'=>$this->input->post('nama_dosen'),
					'jabatan'=>$this->input->post('jabatan'),
					'nip'=>$this->input->post('nip'),
					'deskripsi'=>$this->input->post('deskripsi'),
					'flag'=>$this->input->post('flag'),
					'email'=>$this->input->post('email')
					);
				$data['kode'] = $kode;
				$data['url'] = 'admdosen/edit/'.$kode;
				$data['xinha_inclusion'] = javascript_xinha(array('deskripsi'),
	                                               array('ImageManager',
							'InsertSmiley'),
							"blue-look");
				$this->temp_admin->display('admin/dosen/view_edit',$data);


			} else
			{
				$kunci = $this->input->post('id_key');
				$data['id_dosen'] = $this->input->post('id_dosen');
				$data['nama_dosen'] = $this->input->post('nama_dosen');
				$data['nip'] = $this->input->post('nip');
				$data['jabatan']=$this->input->post('jabatan');
				$data['deskripsi'] = $this->input->post('deskripsi');
				$data['email'] = $this->input->post('email');
				$data['flag'] = $this->input->post('flag');
				$data['operator'] = $this->access->get_user_id();
				if ($gambar['file_name'] != "")
				{
				$data['file'] = $gambar['file_name'];

				}
				$sthapus =  $this->input->post('sthapus');
				if ($sthapus == '1')
				{
				$data['file'] = '';
				}
				$res = $this->model_dosen->update_dosen($data,$kunci);
				$data = array();

				$data['dosen'] = array(
					'id_dosen'=>$this->input->post('id_dosen'),
					'file' => $gambar['file_name'],
					'nama_dosen'=>$this->input->post('nama_dosen'),
					'jabatan'=>$this->input->post('jabatan'),
					'deskripsi'=>$this->input->post('deskripsi'),
					'email'=>$this->input->post('email'),
					'nip'=>$this->input->post('nip'),
					'flag'=>$this->input->post('flag')
					);

				$data['kode'] = $kode;
				if ($res)
				{
				$data['message'] = $this->fungsiku->dialog_sukses('Data berhasil disimpan');
				} else
				{
				$data['message'] = $this->fungsiku->dialog_gagal('Data gagal disimpan');
				}
				$data['xinha_inclusion'] = javascript_xinha(array('deskripsi'),
	                                               array('ImageManager',
							'InsertSmiley'),
							"blue-look");
				$data['url'] = 'admdosen/edit/'.$kode;
				$this->temp_admin->display('admin/dosen/view_edit',$data);

			}
		}
		else
		{

		$data['dosen'] = $this->model_dosen->get_dosen($kode);
		$data['kode'] = '';
		$data['message'] = '';
		$data['url'] = 'admdosen/edit/'.$kode;
		$data['xinha_inclusion'] = javascript_xinha(array('deskripsi'),
	                                               array('ImageManager',
							'InsertSmiley'),
							"blue-look");
		$this->temp_admin->display('admin/dosen/view_edit',$data);
		}
	}

	

}

/* End of file welcome.php */
/* Location: ./application/controllers/administrator.php */
