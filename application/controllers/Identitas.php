<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Identitas extends CI_Controller {

	function __construct(){
		parent:: __construct();
		$this->load->library('template');
    $this->load->model('identitas_model');
    $this->load->model('dosen_model');
    $this->load->model('kuliah_model');
    $this->load->helper('url_helper');
	}

	public function index()
	{
    $data['identitas'] = $this->identitas_model->get_identitas();
		$this->template->display('alumni/index',$data);
	}

  public function view($id = NULL)
  {
    $data['identitas'] = $this->identitas_model->get_identitas($id);
    $data['dosen'] = $this->dosen_model->get_dosen($id);
    $data['kuliah'] = $this->kuliah_model->get_kuliah($id);
		$this->template->display('alumni/view',$data);
  }

  public function create(){
    $this->template->display('alumni/create');
  }

  public function store(){

  }
}
