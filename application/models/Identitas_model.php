<?php
class Identitas_model extends CI_Model{

  public function __construct(){
    $this->load->database();
  }

  public function get_identitas($id = FALSE){

    if($id === FALSE){
      $query = $this->db->get('identitas');
      return $query->result_array();
    }

    $query = $this->db->get_where('identitas', array('id'=>$id));
    return $query->row_array();

  }
}
