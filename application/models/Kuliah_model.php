<?php
class Kuliah_model extends CI_Model{

  public function __construct(){
    $this->load->database();
  }

  public function get_kuliah($id = FALSE){

    $query = $this->db->get_where('kuliah',array('id'=>$id));
    return $query->row_array();

  }
}
