<?php
class Dosen_model extends CI_Model{

  public function __construct(){
    $this->load->database();
  }

  public function get_dosen($id = FALSE){

    $query = $this->db->get_where('dosen',array('id'=>$id));
    return $query->row_array();

  }
}
