<?php
class Tagenda_model extends CI_Model{

  private $tbl = 'tagenda';

  public function __construct(){
    $this->load->database();
  }

  public function get_tagenda($id,$number,$offset){

    if($id===FALSE){
      $query = $this->db->get('tagenda',$number,$offset);
      return $query->result_array();
    }
    $query = $this->db->get_where('tagenda', array('id_agenda'=>$id));
    return $query->row_array();
  }

  public function set_tagenda(){
      $this->load->helper('url');

      $data = array(
        'hari'       => $this->input->post('hari'),
        'tgl_agenda' => $this->input->post('tgl_agenda'),
        'jam'        => $this->input->post('jam'),
        'agenda'     => $this->input->post('agenda'),
        'tempat'     => $this->input->post('tempat'),
        'keterangan' => $this->input->post('keterangan')
      );

      return $this->db->insert('tagenda', $data);
  }

  public function update($id){
    $this->load->helper('url');
    $data = $this->db->where('id_agenda', $id);
    $data = array(
      'hari'       => $this->input->post('hari'),
      'tgl_agenda' => $this->input->post('tgl_agenda'),
      'jam'        => $this->input->post('jam'),
      'agenda'     => $this->input->post('agenda'),
      'tempat'     => $this->input->post('tempat'),
      'keterangan' => $this->input->post('keterangan')
    );
    return $this->db->update('tagenda', $data);
  }

  public function delete($id){
    $data = $this->db->where('id_agenda', $id);
    return $this->db->delete('tagenda',$data);
  }

  public function jumlahData(){
    return $this->db->get('tagenda')->num_rows();
  }
}
