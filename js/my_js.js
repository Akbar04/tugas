jQuery(document).ready(function() {
	jQuery(".slideshow-wrapper").slick({
		autoplay: true,
  		autoplaySpeed: 4000,
  		dots: true
	});

	jQuery('.small-menu-icon').click(function() {
		jQuery('ul.top-menu').slideToggle();
	});

});